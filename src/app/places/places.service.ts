import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places: Place[] = [
    {
      id: 'p1',
      title: 'Casa de pedra',
      description: 'No interior de Sao Chico',
      imgUrl: 'https://s.iha.com/7461700002146/Casas-de-campo-Chassiers-L-Orangerie_2.jpeg',
      price: 149.99
    },
    {
      id: 'p2',
      title: 'Casa de madeira',
      description: 'No centro de Campo Bom',
      imgUrl: 'https://odis.homeaway.com/odis/listing/e7ab1d73-e93c-4a70-862b-29fbe46f6d7c.c10.jpg',
      price: 99.99
    },
    {
      id: 'p3',
      title: 'Casa do nevoeiro',
      description: 'No vale do nevoeiro',
      imgUrl: 'http://st.gde-fon.com/wallpapers_original/433393_utro_dom_pole_tuman_pejzazh_1680x1050_www.Gde-Fon.com.jpg',
      price: 239.99
    }
  ];

  get Places(): Place[] {
    return [...this._places];
  }

  constructor() { }
}
